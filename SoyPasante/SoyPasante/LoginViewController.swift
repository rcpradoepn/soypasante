//
//  LoginViewController.swift
//  SoyPasante
//
//  Created by PRADOPC on 1/15/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import FirebaseAuth
class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        Auth.auth().signIn(withEmail: username, password: password) { (data, error) in
            if let error = error {
                print(error)
                return
            }
            // print("Welcome!")
            self.performSegue(withIdentifier: "to_homeview", sender: self)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
