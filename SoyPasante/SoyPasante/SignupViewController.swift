//
//  SignupViewController.swift
//  SoyPasante
//
//  Created by PRADOPC on 1/15/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import FirebaseAuth
class SignupViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatpasswordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func openLoginViewButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "to_loginview", sender: self)
    }
    
    @IBAction func signupButtonPressed(_ sender: Any) {
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        let repeatpassword = repeatpasswordTextField.text!
        if (isEmailValid(username)) {
            if (password.isEmpty || repeatpassword.isEmpty) {
                print("Campos obligatorios")
            } else {
                if (password == repeatpassword) {
                    if (password.count > 6) {
                        print("valid")
                        Auth.auth().createUser(withEmail: username, password: password) { (data, error) in
                            if let error = error {
                                print(error)
                                return
                            }
                            // print("Welcome!")
                            self.performSegue(withIdentifier: "to_loginview", sender: self)
                        }
                    } else {
                        print("contraseña corta")
                    }
                } else {
                    print("password no coincide")
                }
            }
        } else {
            print("email invalido")
        }
    }
    func isEmailValid(_ value: String) -> Bool {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                return false
            }
        } catch {
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
